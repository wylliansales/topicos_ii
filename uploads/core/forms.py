from django import forms

from uploads.core.models import *


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('description', 'document', 'type')


class ScalingForm(forms.ModelForm):
    class Meta:
        model = Scaling
        fields = ('description', 'fator_escala_x', 'fator_escala_y', 'document')


class TranslationForm(forms.ModelForm):
    class Meta:
        model = Translation
        fields = ('description', 'direcao_x', 'direcao_y', 'document')


class RotationForm(forms.ModelForm):
    class Meta:
        model = Rotation
        fields = ('description', 'document', 'angulo')


class BlurredForm(forms.ModelForm):
    class Meta:
        model = Blurred
        fields = ('description', 'document')


class RealceForm(forms.ModelForm):
    class Meta:
        model = Realce
        fields = ('description', 'document')


class DetectInterestForm(forms.ModelForm):
    class Meta:
        model = DetectInterest
        fields = ('description', 'document', 'type')