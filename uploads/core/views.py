from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from uploads.core.models import *
from uploads.core.forms import *

import numpy as np
import cv2 as cv
from PIL import Image


def home(request):
    documents = Document.objects.all()
    return render(request, 'core/home.html', { 'documents': documents })


def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'core/simple_upload.html')


def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')

            # Escala é apenas redimensionar a imagem. O OpenCV vem com a função cv.resize () para essa finalidade.
            # O tamanho da imagem pode ser especificado manualmente ou você pode especificar o fator de escala.
            if request.POST['type'] == 'SC':
                img = cv.imread('././.' + form.instance.document.url)
                res = cv.resize(img, None, fx=9, fy=2, interpolation=cv.INTER_CUBIC)

                # height, width = img.shape[:2]
                # res = cv.resize(img, (2 * width, 2 * height), interpolation=cv.INTER_CUBIC)

            # mudança da localização do objeto.
            # Transformá-lo em uma matriz Numpy do tipo np.float32 e passá-lo para a função cv.warpAffine ()
            # Matriz de transformação
            elif request.POST['type'] == 'TR':
                img = cv.imread('././.' + form.instance.document.url, 0)
                rows, cols = img.shape
                M = np.float32([[1, 0, 100], [0, 1, 50]])
                # O terceiro argumento da função cv.warpAffine () é o tamanho da imagem de saída
                res = cv.warpAffine(img, M, (cols, rows))

            elif request.POST['type'] == 'RO':
                img = cv.imread('././.' + form.instance.document.url, 0)
                rows, cols = img.shape
                # cols-1 and rows-1 are the coordinate limits.
                M = cv.getRotationMatrix2D(((cols - 1) / 2.0, (rows - 1) / 2.0), 90, 1)

                res = cv.warpAffine(img, M, (cols, rows))

            elif request.POST['type'] == 'BU':
                img = cv.imread('././.' + form.instance.document.url)
                res = cv.blur(img, (5, 5))

            elif request.POST['type'] == 'RA':
                img = cv.imread('././.' + form.instance.document.url)
                img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
                img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
                res = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)

            cv.imwrite("./././media/imagem/" + name_file, res)
            Document.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
            return redirect('home')
    else:
        form = DocumentForm()
    return render(request, 'core/model_form_upload.html', {
        'form': form
    })


def scaling(request):
    if request.method == 'POST':
        form = ScalingForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')
            img = cv.imread('././.' + form.instance.document.url)

            # res = cv.resize(img, None, fx=form.instance.x, fy=form.instance.y, interpolation=cv.INTER_CUBIC)
            height, width = img.shape[:2]
            res = cv.resize(img, (form.instance.fator_escala_x * width, form.instance.fator_escala_x * height), interpolation=cv.INTER_CUBIC)

            cv.imwrite("./././media/imagem/" + name_file, res)
            Scaling.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
            return redirect('home')
    else:
        form = ScalingForm()
    return render(request, 'core/model_form_upload.html', {'form': form})


def translation(request):
    if request.method == 'POST':
        form = TranslationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')
            img = cv.imread('././.' + form.instance.document.url)

            rows, cols = img.shape
            M = np.float32([[1, 0, 100], [0, 1, 50]])
            # O terceiro argumento da função cv.warpAffine () é o tamanho da imagem de saída
            res = cv.warpAffine(img, M, (cols, rows))

            cv.imwrite("./././media/imagem/" + name_file, res)
            Translation.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
            return redirect('home')
    else:
        form = TranslationForm()
    return render(request, 'core/model_form_upload.html', {'form': form})


def rotation(request):
    if request.method == 'POST':
        form = RotationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')
            img = cv.imread('././.' + form.instance.document.url, 0)
            rows, cols = img.shape
            # cols-1 and rows-1 are the coordinate limits.
            M = cv.getRotationMatrix2D(((cols - 1) / 2.0, (rows - 1) / 2.0), form.instance.angulo, 1)

            res = cv.warpAffine(img, M, (cols, rows))
            cv.imwrite("./././media/imagem/" + name_file, res)
            Rotation.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
            return redirect('home')
    else:
        form = RotationForm()
    return render(request, 'core/model_form_upload.html', {'form': form})


def blurred(request):
    if request.POST == 'POST':
        form = BlurredForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')
            img = cv.imread('././.' + form.instance.document.url)
            res = cv.blur(img, (5, 5))

            cv.imwrite("./././media/imagem/" + name_file, res)
            Blurred.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
            return redirect('home')
    else:
        form = BlurredForm()
    return render(request, 'core/model_form_upload.html', {'form': form})


def realce(request):
    if request.POST == 'POST':
        form = RealceForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')
            img = cv.imread('././.' + form.instance.document.url)
            img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
            img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
            res = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)

            cv.imwrite("./././media/imagem/" + name_file, res)
            Realce.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
            return redirect('home')
    else:
        form = RealceForm()
    return render(request, 'core/model_form_upload.html', {'form': form})


def detect_interest(request):
    if request.POST == 'POST':
        form =  DetectInterestForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            name_file = form.instance.document.name.replace('documents/', '')
            img = cv.imread('././.' + form.instance.document.url)
            if request.POST['type'] == 'SIF':
                sift = cv.SIFT()
                kp = sift.detect(gray, None)
                # img2 =cv.drawKeypoints(gray, kp)
                img2 =cv.drawKeypoints(gray,kp,flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
                cv.imwrite("./././media/imagem/" + name_file, img2)
                DetectInterest.objects.filter(pk=form.instance.id).update(imagem="imagem/" + name_file)
                return redirect('home')
            # elif request.POST['type'] == 'SUF':
                
    else:
        form = DetectInterestForm()
        return render(request, 'core/model_form_upload.html', {'form': form})

                
