from __future__ import unicode_literals

from django.db import models


class Document(models.Model):
    SCALING = 'SC'
    TRANSLATION = 'TR'
    ROTATION = 'RO'
    BLURRED = 'BU'
    REALCE = 'RA'
    TYPE_CHOICES = [
        (SCALING, 'Alterar escala'),
        (TRANSLATION, 'Translação'),
        (ROTATION, 'Rotação'),
        (BLURRED, 'Desfocar'),
        (REALCE, 'Realce')
    ]
    description = models.CharField(max_length=255, blank=True)
    type = models.CharField(
        max_length=2,
        choices=TYPE_CHOICES,
        default=SCALING
    )
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Scaling(models.Model):
    description = models.CharField(max_length=255, blank=True)
    fator_escala_x = models.IntegerField(null=False)
    fator_escala_y = models.IntegerField(null=False)
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Translation(models.Model):
    description = models.CharField(max_length=255, blank=True)
    direcao_x = models.IntegerField(null=False)
    direcao_y = models.IntegerField(null=False)
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Rotation(models.Model):
    description = models.CharField(max_length=255, blank=True)
    angulo = models.IntegerField(null=False)
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Blurred(models.Model):
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Realce(models.Model):
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)


class DetectInterest(models.Model):
    SUFT = 'SUF'
    SIFT = 'SIF'

    TYPE_CHOICES = [
        (SUFT, 'Suft'),
        (SIFT, 'Sift')
    ]

    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    imagem = models.FileField(upload_to='image/', null=True)
    type = models.CharField(
        max_length=2,
        choices=TYPE_CHOICES,
        default=SUFT
    )
    uploaded_at = models.DateTimeField(auto_now_add=True)